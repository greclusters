import os, pickle

class DiskCache:

    instances = set()

    def __init__(self, name):
        self.instances.add(self)
        self.name = name
        self.filename = "cache_%s" % name
        self.items = {}
        if os.path.exists(self.filename):
            self.items.update(pickle.load(open(self.filename)))

    def __contains__(self, key):
        return key in self.items

    def __getitem__(self, key):
        return self.items[key]
    
    def __setitem__(self, key, value):
        self.items[key] = value

    def flush(self):
        pickle.dump(self.items, open(self.filename, "w"))

def cached(func):
    CACHE = DiskCache(func.__name__)
    def pre_func(*args):
        if args not in CACHE:
            CACHE[args] = func(*args)
        return CACHE[args]
    return pre_func

def flush_cache():
    "flush all cache to disk"
    for c in DiskCache.instances:
        c.flush()

__all__ = ['cached', 'flush_cache']
