#! /usr/bin/python

# Use this program in conjuction with the GRE word list file (You must
# be using it along with the `wl-mode' emacs major mode too).  This
# programs runs on both Windows XP and Linux and periodically shows
# random words with their meanings in the systray.

from __future__ import with_statement

import os, random, pickle, datetime
from contextlib import contextmanager

__author__ = "Sridhar Ratna"

WORD_LIST = "33new.txt.wl"
DATA_FILE = "data"

SHOWN_WORDS = {}

@contextmanager
def pickled(filename):
    if os.path.isfile(filename):
        data = pickle.load(open(filename))
    else:
        data = {}

    def getter(item, type):
        if item in data:
            return data[item]
        else:
            data[item] = type()
            return data[item]

    yield getter

    pickle.dump(data, open(filename, "w"))

def write_stats():
    cnt = {}
    for line in open(WORD_LIST):
        for mark in "-+* ":
            if line.startswith("%s " % mark):
                cnt[mark] = cnt.get(mark, 0) + 1
    now = datetime.datetime.now()
    with pickled(DATA_FILE) as p:
        log = p("wl-count-log", list)
        if not log or log[-1][1] != cnt:
            log.append((now, cnt))

def random_word():
    words = []
    for line in open(WORD_LIST):
        # Only words marked with `*' are considered!
        if line.startswith("* ") or line.startswith(" "):
            word = line.split(None, 2)[1:]
            words.append(word)
    random.seed()
    while True:
        word, meaning = random.choice(words)
        if word not in SHOWN_WORDS:
            SHOWN_WORDS[word] = True
            return word, meaning

if os.name == 'nt':
    # Windows XP GUI
    import win32gui, win32con
    from taskbardemo import Taskbar
    
    class DemoTaskbar(Taskbar):

        def __init__(self):
            Taskbar.__init__(self)
            icon = win32gui.LoadIcon(0, win32con.IDI_APPLICATION)
            self.setIcon(icon)
            self.show()
            
        def onClick(self):
            word, meaning = random_word()
            self.showBalloon(word, meaning)
            write_stats()

        def onDoubleClick(self):
            print "you double clicked, bye!"
            win32gui.PostQuitMessage(0)

        def onRightClick(self):
            self.showBalloon("Hello!", "Left-click to see a new word")

        def showBalloon(self, title, msg):
            flags = win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_INFO
            nid = (self.hwnd, 0, flags, win32con.WM_USER+20,
                   self.hicon, "", msg, 10, title,
                   win32gui.NIIF_INFO)
            #change our already present icon ...
            win32gui.Shell_NotifyIcon(win32gui.NIM_MODIFY, nid)
            
            
    t = DemoTaskbar()
    win32gui.PumpMessages() # start demo
    
elif os.name == 'linux':
    # GNOME Applet GUI
    import pygtk
    pygtk.require("2.0")
    import gtk, gobject
    import egg.trayicon

    def next_word(label, tips, t):
        word = random_word()
        print word
        label.set_text(word[0])
        tips.set_tip(t, word[1].strip())
        return True

    def clicked(_, __, *args):
        next_word(*args)

    def timeout(*args):
        write_stats()
        return next_word(*args)

    def mins(n):
        "Return `n' secs in millisecs"
        return n*60*1000

    def setup_ui():
        t = egg.trayicon.TrayIcon("GRE Word List Applet")
        l = gtk.Label()
        l.set_selectable(True)
        t.add(l)

        tips = gtk.Tooltips()
        tips.enable()

        l.connect("button-press-event", clicked, l, tips, t)
        next_word(l, tips, t)

        gobject.timeout_add(mins(5), timeout, l, tips, t)

        t.show_all()
        gtk.main()

    setup_ui()
else:
    raise RuntimeError, "Unsupported OS"


