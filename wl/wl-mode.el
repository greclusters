;;; wl-mode -- Major mode for studying GRE Word Lists

;; Author: Sridhar Ratna <http://srid.nfshost.com/>
;; Created: 
;; Keywords: GRE Word List major-mode

;; Copyright (C) 2007 Sridhar Ratna

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public
;; License along with this program; if not, write to the Free
;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
;; MA 02111-1307 USA

;; TODO
;;  - documentation + sample word lists (barrons)
;;  - custom font faces

(defvar wl-mode-hook nil)

(defun wl-mark (ch)
  (wl-unmark)
  (beginning-of-line)
  (subst-char-in-region (point) (+ 1 (point)) ?  ch))

; Difficult word; `being' learned
(defun wl-mark-star ()
  (interactive)
  (wl-mark ?*))

; Difficult word; `able' to recall
(defun wl-mark-plus ()
  (interactive)
  (wl-mark ?+))

; Easy word; always remembering
(defun wl-mark-dash ()
  (interactive)
  (wl-mark ?-))

(defun wl-unmark ()
  (interactive)
  (beginning-of-line)
  (subst-char-in-region (point) (+ 1 (point)) ?* ? )
  (subst-char-in-region (point) (+ 1 (point)) ?+ ? )
  (subst-char-in-region (point) (+ 1 (point)) ?- ? ))

(defun wl-goto-random ()
  (interactive)
  (random t)
  (goto-line (+ 1
		(random (count-lines (point-min)
				     (point-max))))))

(defvar wl-mode-map
  (let ((wl-mode-map (make-keymap)))
    (define-key wl-mode-map "\C-c\C-u" 'wl-unmark)
    (define-key wl-mode-map (kbd "<f4>") 'wl-mark-star)
    (define-key wl-mode-map (kbd "C-<f4>") 'wl-mark-plus)
    (define-key wl-mode-map "\C-c\C-r" 'wl-mark-dash)
    (define-key wl-mode-map (kbd "<f5>") 'wl-goto-random)
    wl-mode-map)
  "Keymap for WL major mode")

;(add-to-list 'auto-mode-alist '("\\.wl\\'" . wl-mode))

(defconst wl-font-lock-keywords-1
  (list
   '("^\\*.[a-z]+" . font-lock-keyword-face)
   '("^-.[a-z]+" . font-lock-comment-face)
   '("^+.[a-z]+" . font-lock-comment-face)
   '("^..[a-z]+" . font-lock-constant-face))
  "Highlighting for WL major mode")

(defvar wl-font-lock-keywords wl-font-lock-keywords-1
   "Default highlighting expressions for WL mode")


(defun wl-mode ()
  "Major mode for studying GRE Word Lists"
  (interactive)
  (kill-all-local-variables)
  (use-local-map wl-mode-map)
  (set (make-local-variable 'font-lock-defaults) '(wl-font-lock-keywords))
  (global-hl-line-mode 1) ;; FIXME: this is not `local' to the buffer
  (setq major-mode 'wl-mode)
  (setq mode-name "Word List")
  (run-hooks 'wl-mode-hook))

(provide 'wl-mode)