#!/usr/bin/python
import pickle, datetime

today = datetime.datetime.now().date()

total = {}

for dt, cnt in pickle.load(open("data"))["wl-count-log"]:
    consolidated = sum([cnt[c] for c in "-+*"])
    total[ dt.date() ] = consolidated

print cnt

tot = total.items()
tot.sort()

for date, cnt in tot:
    delta = None
    prev = date-datetime.timedelta(1)
    prev_cnt = total.get(prev)
    if prev and prev_cnt:
        delta = cnt - prev_cnt
    print date, delta, cnt


