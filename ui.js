function random_cluster(till){
    return function(){
	var n = Math.floor(Math.random()*till);
	window.location = "#c" + n;
    }
}

function scroller(offset, repeat_code){
    var MAX_STEPS = 10;
    var INCREMENT = 10;
    var REPEAT_FREQ = 30;
    
    var timer;
    var steps=0;
    
    function scrollABit(){
        if (steps > MAX_STEPS){
            clearTimeout(timer);
	    steps = 0;
        }else{
            window.scrollBy(0, INCREMENT * offset);
            steps = steps + 1;
            timer = setTimeout(repeat_code, REPEAT_FREQ);
        }
    }
    return scrollABit;
}

var scroll_down = scroller(+1, "scroll_down()");
var scroll_up = scroller(-1, "scroll_up()");

function scroll_to(index){
    return function(){
	window.location = "#c" + index;
    }
}

$(document).shortkeys({
    'n': scroll_down,
    'p': scroll_up,
    'r': random_cluster(total),
    'u': random_cluster(200),
    'i': random_cluster(500),
    '0': scroll_to("1"),
    '1': scroll_to("100"),
    '2': scroll_to("200"),
    '3': scroll_to("300"),
    '4': scroll_to("400"),
    '5': scroll_to("500"),
    '6': scroll_to("600"),
    '7': scroll_to("700"),
    '8': scroll_to("800"),
    '9': scroll_to("900"),
});


