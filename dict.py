import re
from nltk.wordnet import N,V,ADJ,ADV

from cache import cached

parts = [N, V, ADJ, ADV]

@cached
def find_definition(word):
    # TODO: do fuzzy matching for misspelled words and its variations
    s = ""
    for part in parts:
        if word in part:
            s += "[%s]\n" % part.pos
            for sense in part[word]:
                s += sense.gloss + "\n"
            s += "\n"
    return s

pat_quoted = re.compile('("[^"]*")')
@cached
def find_definition_html(word):
    """This is much like `find_definition_html' except the returned
    string is formatted as pretty HTML
    """
    s = ""
    for part in parts:
        if word in part:
            s += "<p><b>[%s]</b><ol>" % part.pos
            for sense in part[word]:
                s += "<li>" + pat_quoted.sub(r'<i>\1</i>', sense.gloss)
                s += "</li>"
            s += "</ol></p>"
    return s

if __name__ == '__main__':
    print "dog:\n", find_definition("dog")
    print "mitigate:\n", find_definition("mitigate")
