# parse MAMA easy format (pdf2txt)

from __future__ import with_statement

import re, time
import simplejson

from genshi.template import MarkupTemplate
from genshi.core import Markup

from dict import find_definition_html
from cache import flush_cache

def e_clusters():
    p = re.compile("\d+,[\w-]+(( |\()+.*\n)*")
    s = open("GRE_Verbal.txt").read()
    for m in p.finditer(s):
        yield m.group()

def parse():
    for c in e_clusters():
        # print '#', c, '<<'
        index, c = c.split(",", 1)
        assert int(index) > 0, "%s is not valid +ve integer?" % index
        word, c = c.split(" ", 1)
        word = word.strip()
        synonyms, antonyms = [], []
        for line in c.splitlines():
            # we do not split by a single SPACE
            # lest we should split the synonym "phrases"
            # print '$', line
            line = line.strip()
            if "  " in line:
                syn, ant = line.split("  ", 1)
            else:
                syn, ant = line, ""
            for g, e in [(synonyms, syn), (antonyms, ant)]:
                if e:
                    if ',' in e:
                        g.extend([s.strip().lower() for s in e.split(',')])
                    else:
                        g.append(e.strip().lower())

        yield word.lower(), synonyms, antonyms

clusters = {}
for w, s, a in parse():
    assert w not in clusters, "%s is already in cluster!" % w
    clusters[w] = (s, a)

def print_words_json():
    words = clusters.keys()
    words.sort(key=str.lower)
    with open("words.js", "w") as f:
        f.write("var words = %s;\n" % simplejson.dumps(words))
        f.write("var clusters = %s;\n" % simplejson.dumps(clusters))

pat_parenthesis = re.compile('(\([^)]*\))')
def fuzzy_match_definition(phrase):
    phrase = pat_parenthesis.sub('', phrase).strip() # remove ()'ed text
    if not phrase: return '' # ???
    return find_definition_html(phrase)

def def_dict():
    words = clusters.keys()
    dict = {}
    def add(w):
        if w and w not in dict:
            dict[w] = fuzzy_match_definition(w)
            
    for word in words:
        add(word)
        [add(w) for w in clusters[word][0]]
        [add(w) for w in clusters[word][1]]

    return dict

def map_keys(d):
    "map keys of `d' to unique integer and return the mapping"
    keydict = {}
    next_key = 1
    for k in d:
        if k not in keydict:
            keydict[k] = next_key
            next_key += 1
    return keydict

class erosive_dict(object):
    "__contains__ returns False for elements that were read at least once"

    def __init__(self, d):
        self.d = d
        self.read = {}

    def __getitem__(self, key):
        self.read[key] = True
        return self.d[key]

    def __contains__(self, key):
        return key not in self.read and key in self.d

    def __getattr__(self, a):
        return self.d.__getattr__(a)

def print_index_html():
    tmpl = MarkupTemplate(open("index.tmpl.html").read())
    words = clusters.keys()
    words.sort()
    defdict = def_dict()
    defdict[""] = "naaaaaa" # ??
    stream = tmpl.generate(clusters=clusters,
                           words=words,
                           defdict=defdict,
                           keydict=erosive_dict(map_keys(defdict)),
                           Markup=Markup)
    with open("index.html", "w") as f:
        f.write(stream.render())
        
print_index_html()
flush_cache()

    
